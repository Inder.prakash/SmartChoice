package com.hibernate;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hibernate.cart.Cart;
import com.hibernate.cart.CartDAO;
import com.hibernate.categories.CategoriesDAO;
import com.hibernate.customer.Customers;
import com.hibernate.customer.CustomersDAO;
import com.hibernate.products.LatestproductDAO;
import com.hibernate.products.ProductsDAO;
import com.sun.mail.smtp.SMTPTransport;

@Controller
public class SmartChoiceController {
	@Autowired
	CustomersDAO custdao;
	@Autowired
	ProductsDAO pdao;
	@Autowired
	CategoriesDAO cdao;
	@Autowired
	LatestproductDAO ldao;
	@Autowired
	CartDAO cartdao;
	
	
	public String LoginTest()
	{
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null && !auth.getName().equals("anonymousUser"))
	    {    
	    	return "true";
	    }	
		return "false";
	}

	@RequestMapping("/")
	public ModelAndView landingpage() {
        ModelAndView mv = new ModelAndView("redirect:/index");
		return mv;
	}
	
	@RequestMapping("/index")
	public ModelAndView index() {
        ModelAndView mv = new ModelAndView("index");       
        mv.addObject("category",cdao.getAllCategories());
        mv.addObject("latestproduct",ldao.getLatestProduct());
		return mv;
	}
	
	@RequestMapping(value="/loginpage")
	public ModelAndView login() throws IOException{	
		ModelAndView mv = new ModelAndView("login");	
		return mv ;
	}
	
	@RequestMapping(value="/product")
	public ModelAndView product() throws IOException{	
		ModelAndView mv = new ModelAndView("product");	
		return mv ;
	}
	
	
	@RequestMapping("/SignUP")
	public ModelAndView SignUp() {
		ModelAndView mv = new ModelAndView("SignUp");
		mv.addObject("Customers", new Customers());
		return mv;
	}
	
	@RequestMapping("/reg/{msg}")
	public ModelAndView reg(@PathVariable("msg") String pass) {
		ModelAndView mv = new ModelAndView("SignUp");
		mv.addObject("Customers", new Customers());
		mv.addObject("pass",pass);
		return mv;
	}
	
	@RequestMapping(value="/AddCustomerToDb",method=RequestMethod.POST)
	public ModelAndView AddCustomerToDb( HttpServletRequest req , @Valid @ModelAttribute("Customers") Customers cust,BindingResult result ) {	
		if(result.hasErrors()) {
			System.out.println("FAILED");
			ModelAndView mv = new ModelAndView("redirect:/SignUP");		
			return mv;		
		}	
		else{
		 	 this.custdao.insert(cust);
		 	 System.out.println("PASS");
		 	 ModelAndView mv = new ModelAndView("redirect:/reg/pass");		
			 return mv;
		}
			
	}
	
	@RequestMapping("/UpdateProfile/{id}")
	public ModelAndView UpdateProfile(@PathVariable("id")int id) {
		ModelAndView mv = new ModelAndView("UpdateProfile");
		mv.addObject("Customers", custdao.getCustomer(id));
		return mv;
	}
	

	@RequestMapping(value="/UpdateProfileToDB",method=RequestMethod.POST)
	public ModelAndView UpdateProfileToDB( HttpServletRequest req , @Valid @ModelAttribute("profiledata") Customers cust , BindingResult resultset ) 
	{
		ModelAndView mv = new ModelAndView("redirect:/MyProfiles/"+req.getParameter("id"));	
		this.custdao.update(cust);
		return mv;
	}
	
	@RequestMapping("/MyProfiles/{id}")
	public ModelAndView MyProfiles(@PathVariable("id")int id) {
		ModelAndView mv = new ModelAndView("MyProfiles");
		mv.addObject("Customers", custdao.getCustomer(id));
		return mv;
	}
	
	@RequestMapping("/ViewProduct/{id}")
	public ModelAndView ViewProduct(@PathVariable("id")int catid) {
		ModelAndView mv = new ModelAndView("ViewProduct");	
		mv.addObject("productdata", pdao.getSelectedProducts(catid));
		return mv;
	}
	
	@RequestMapping("/productbysubcat/{id}")
	public ModelAndView productbysubcat(@PathVariable("id")int subid) {
		ModelAndView mv = new ModelAndView("ViewProduct");	
		mv.addObject("productdata", pdao.getbysubcat(subid));
		return mv;
	}
	
	@RequestMapping("/product/{id}")
	public ModelAndView product( @PathVariable("id")int catid , Principal p , HttpSession session) {
		ModelAndView mv = new ModelAndView("product");	
		if ( session.getAttribute("userName") != null ) {
			mv.addObject("user",custdao.getCustomer( p.getName().toString() ) );
		}
		mv.addObject("productdata", pdao.getProduct(catid));
		return mv;
	}
	
	@RequestMapping(value="/Searchdata",method=RequestMethod.POST)
	public ModelAndView Searchdata(@RequestParam("keytag") String keytag) {
		ModelAndView mv = new ModelAndView("searchresult");	
		mv.addObject("Searchdata", pdao.getSearchdata(keytag));
		return mv;
	}
	
	@RequestMapping(value="/AddCartToDB",method=RequestMethod.POST)
	public ModelAndView AddCartToDB( @RequestParam("address")String address , @RequestParam("quantity") String q ,@RequestParam("id")int id , @RequestParam("userid")int userid ) {
		
		int quantity = Integer.parseInt(q);
		ModelAndView mv = new ModelAndView("redirect:/cart");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && !auth.getName().equals("anonymousUser")) {
			boolean found = false;
			int i = -1;
			for( Cart c1 : cartdao.getProductsbycustid(userid) )
			{
				if( c1.getCustomerid() == userid && c1.getProductid()==id  )
				{
					found = true;
					i = c1.getId();
					break;
			    					
				}
				
			}
			if( found )
			{
				Cart c = cartdao.getCartbyId(i);
				
	            c.setQuantity(quantity);
				this.cartdao.update(c);
				return mv;

			}
			
			Cart c = new Cart();
			
			c.setCustomerid(userid);
			c.setProductid(id);
			c.setQuantity(quantity);
			c.setBillingaddress(address);
			c.setShippingaddress(address);
			
			this.cartdao.insert(c);		
		}
		return mv;
	}
	
	@RequestMapping("/mail")
	public ModelAndView mail(HttpServletRequest req , HttpServletResponse response) throws AddressException, MessagingException, IOException {
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String description = req.getParameter("comments");

		String message = "<table>"
				+ "<tr><td>Name : "+name+"</td></tr>"
						+ "<tr><td><div style='background-color:#F0F8FF;'>Your message : "+description+"</div></td></tr>"
								+ "<tr><td>Thank you for visting us. we will contact you shortly</td></tr>"
											+ "</table>";
				
		 mailsending(email, "Smart@Choice", message);
		 ModelAndView mv = new ModelAndView("index");       
	     mv.addObject("category",cdao.getAllCategories());
	     mv.addObject("latestproduct",ldao.getLatestProduct());
	     mv.addObject("msg","mails");
         return mv;
	}
	
	public void mailsending(String To, String Sub , String Messagetext) throws AddressException, MessagingException {
		Properties props = System.getProperties();
        props.put("mail.smtps.host","smtp.gmail.com");
        props.put("mail.smtps.auth","true");
        Session session = Session.getInstance(props, null);
        Message msg = new MimeMessage(session);     
        msg.setFrom(new InternetAddress("devopsjava@gmail.com"));;
        msg.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse(To, false));
        msg.setSubject(Sub);
        msg.setContent(Messagetext, "text/html; charset=utf-8");
        msg.setSentDate(new Date());
        SMTPTransport t =
            (SMTPTransport)session.getTransport("smtps");
        t.connect("smtp.gmail.com", "devopsjava@gmail.com", "9716923847");
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
	}
}


