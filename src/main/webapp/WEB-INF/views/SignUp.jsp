<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="header.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/resources/js/SignUpController.js"></script>

<div ng-app="myapp" ng-controller="SignUpController">
<br>

<form:form method="post" action="AddCustomerToDb" style="width: 20%;margin: auto;" modelAttribute="Customers">
  <div class="form-group">
    <label>First Name :</label>
    <form:input path="firstname" ng-change="firstname.validate()" ng-model="firstname.value"  type="text" placeholder="First Name.... " class="form-control" />
  	 <span style="color:red;" ng-if="firstname.touched && firstname.error" class="help-block">First Name can't be empty.</span>
     <span style="color:red;" ng-if="firstname.touched && firstname.length" class="help-block">First Name must be between 3 and 8 characters </span>
  </div>
  
   <div class="form-group">
    <label>Last Name :</label>
    <form:input path="lastname" ng-change="lastname.validate()" ng-model="lastname.value" type="text" placeholder="Last Name.... " class="form-control" />
  	 <span style="color:red;" ng-if="lastname.touched && lastname.error" class="help-block">Last Name can't be empty.</span>
     <span style="color:red;" ng-if="lastname.touched && lastname.length" class="help-block">Last Name must be between 3 and 8 characters </span>
  </div>
  
  <div class="form-group">
    <label>Email :</label>
    <form:input name="email" ng-blur="chk()" ng-change="email.validate()" ng-model="email.value" path="email" type="text" placeholder="Email Addresss...." class="form-control" />
    <span style="color:red;" ng-if="email.touched && email.error" class="help-block">Invalid Email ID</span>
    <span style="color:red;" ng-if="email.touched && email.empty" class="help-block">Email can't be empty</span>
    <span style="color:red;" ng-if="email.touched && email.chk" class="help-block">Email Already Exists.</span>
  </div>
  
  <div class="form-group">
    <label>Password :</label>
    <form:input name="pass" ng-change="password.validate()" ng-model="password.value" path="password" type="password" placeholder="Password" class="form-control" />
     <form:errors cssStyle="color:red" path="password"/>
     <span style="color:red;" ng-if="password.touched && password.error" class="help-block">
     Minimum 8 characters at least one uppercase letter,one lowercase letter,
     one number and one special character</span>
     <span style="color:red;" ng-if="password.touched && password.empty" class="help-block">Password can't be empty</span>
  </div>
  
  <div class="form-group">
    <label>Confirm Password :</label>
    <input required="required" ng-model="cpassword.value" ng-change="cpassword.validate()" name="cpass" type="password" placeholder="Confirm Password" class="form-control">
     <span style="color:red;" ng-if="cpassword.touched && cpassword.match" class="help-block">Password Does not match.</span>
  </div>
  
  <div class="form-group">
    <label>Phone :</label>
    <form:input path="phone" ng-model="phone.value" ng-change="phone.validate()"  type="text" placeholder="Phone" class="form-control" />
     <span style="color:red;" ng-if="phone.touched && phone.error" class="help-block">Invalid Phone Number</span>
     <span style="color:red;" ng-if="phone.touched && phone.empty" class="help-block">Phone number can't be empty</span>
  </div>
  
  <div class="form-group">
    <label>First Address :</label>
    <form:textarea path="address1" ng-model="faddress.value" ng-change="faddress.validate()" rows="5" cols="5" placeholder="Primary Address...." class="form-control" ></form:textarea>
    <span style="color:red;" ng-if="faddress.touched && faddress.empty" class="help-block">First Address can't be empty</span>
  </div>
  
  <div class="form-group">
    <label>Second Address :</label>
    <form:textarea path="address2" rows="5" cols="5" placeholder="Second Address (OPTIONAL)" class="form-control" ></form:textarea>  
  </div>
  
  <div class="form-group">
  <label>Select City :</label>
  <form:select class="form-control" ng-model="city.value" ng-change="city.validate()" path="city" >
  <option value="Mumbai">Mumbai</option>
  <option value="Delhi">Delhi</option>
  <option value="Gujarat">Gujarat</option>
  <option value="Uttar Pradesh">Uttar Pradesh</option>
  <option value="Punjab">Punjab</option>
  <option value="Rajasthan">Rajasthan</option>  
  </form:select>
   <span style="color:red;" ng-if="city.touched && city.error" class="help-block">Select City</span>
  </div>
  
  <div class="form-group">
    <label>Enter Pin-code :</label>
    <form:input path="pincode" ng-model="pin.value" ng-change="pin.validate()" type="number" maxlength="6"  placeholder="Pin code...." class="form-control" />
    <span style="color:red;" ng-if="pin.touched && pin.error" class="help-block">Invalid Pin code</span>
     <span style="color:red;" ng-if="pin.touched && pin.empty" class="help-block">Enter Pin code ...</span>
  </div>
  
  <div style="text-align: center;" class="form-group">
  <button type="submit" ng-disabled=
                        '!(  
                         !firstname.error && !firstname.length && firstname.touched &&
                         !lastname.error && !lastname.length && lastname.touched &&
                         !email.error && !email.empty && email.touched && !email.chk &&
                         !password.empty && !password.error && password.touched &&
                         cpassword.touched && !cpassword.match  &&
                         !pin.error && !pin.empty && pin.touched &&
                         faddress.touched && !faddress.empty &&
                         phone.touched && !phone.empty && !phone.error &&
                         city.touched && !city.error
                        )'
                         class="btn btn-primary">Submit</button>
  </div>

</form:form>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
			<c:if test="${not empty pass}">
	  		 <script type="text/javascript">
	  		 	swal("Registration Successfully Complete");
	  		 </script>
			</c:if>
</div>
<Style>
body { 
    background: linear-gradient(to bottom, #abbaab 0%, #ffffff 100%);
    height: 100%; 
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</Style>
<br><br><br><br>
<div  id="body">
<!-- Container (Services Section) -->
<div style="color: white;" class="container-fluid text-center">
 <br> <br><br>
  <img src="${pageContext.request.contextPath}/resources/images/services.png">
  <h4>What we offer</h4>
  <br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-off logo-small"></span>
      <h4>POWER</h4>
      <p>Best manpower quotes selected by thousands of our users!</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-heart logo-small"></span>
      <h4>LOVE</h4>
      <p>Showing customers the love and respect they deserve has never been more critical.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-lock logo-small"></span>
      <h4>JOB DONE</h4>
      <p>Choose a job you love, and you will never have to work a day in your life.</p>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-leaf logo-small"></span>
      <h4>GREEN</h4>
      <p>The future will either be green or not at all.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-certificate logo-small"></span>
      <h4>CERTIFIED</h4>
      <p>Officially recognize as possessing certain qualifications or meeting certain standards.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-wrench logo-small"></span>
      <h4>HARD WORK</h4>
      <p>Some people dream of success while others wake up and work hard at it.</p>
    </div>
  </div>
</div>


<!-- Container (Contact Section) -->
<div style="color: white;" class="container-fluid bg-grey">
<div class="container">
<br>
  <h2 style="margin-top: 80px;" class="text-center">CONTACT</h2>
  <div " class="row">
    <div class="col-sm-5">
    <br><br>
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> India , New Delhi</p>
      <p><span class="glyphicon glyphicon-phone"></span>97169*****</p>
      <p><span class="glyphicon glyphicon-envelope"></span> Inder*****@hotmail.com</p> 
    </div>
    <div class="col-sm-7">
    <form id="mailform" action="mail" method="post">
      <div class="row">
      <br><br>
      
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <div class="row">
      	<div class="col-sm-12 form-group">
      		<textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
      	</div> 
      </div>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button type="submit" form="mailform" class="btn btn-info submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Send</button>
      	</div> 
      </div> 	
      </form>
    </div>
  </div>
  </div>
</div>
<br>
<br>
<br>
<br>
</div>