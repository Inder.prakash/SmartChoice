<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="${pageContext.request.contextPath}/resources/css/style-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet" />
<title>SmartChoice</title>
</head>
<body  id="body" >
<jsp:include page="header.jsp" />  
 <div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      <li data-target="#myCarousel" data-slide-to="7"></li>
      <li data-target="#myCarousel" data-slide-to="8"></li>
    </ol>

 
    <div class="carousel-inner">
      <div class="item active">
        <img src="resources/images/Coursel1.jpg" alt="1" style="width:100%;">
      </div>

      <div class="item">
        <img src="resources/images/Coursel2.jpg" alt="2" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="resources/images/Coursel3.jpg" alt="3" style="width:100%;">
      </div>
        
      <div class="item">
        <img src="resources/images/Coursel4.jpg" alt="4" style="width:100%;">
      </div>
        
      <div class="item">
        <img src="resources/images/Coursel5.jpg" alt="5" style="width:100%;">
      </div>
      
      <div class="item">
        <img src="resources/images/Coursel6.jpg" alt="6" style="width:100%;">
      </div>
      
      <div class="item">
        <img src="resources/images/Coursel7.jpg" alt="7" style="width:100%;">
      </div>
      
      <div class="item">
        <img src="resources/images/Coursel8.jpg" alt="8" style="width:100%;">
      </div>
      
      <div class="item">
        <img src="resources/images/Coursel9.jpg" alt="9" style="width:100%;">
      </div>
      
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>

   <div style="height:20px; " class="row">
        <div class="col-sm-4"></div>
    </div>
  </div>
   
  <div style="width: 100%;" class="row catimage-div">
    <c:forEach items="${category}" var="x">   
    <div align="center" class="col-sm-3">
      <a href="ViewProduct/${x.getId()}"><img src="${x.getCategoryimage()}" class="img-responsive indexcatimage" ></a>
    </div>
    </c:forEach>
  </div>
  
 
  
  
  <div style="width: 100%;" class="row catimage-div"> 
    <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/1">
      <img class="imgrotate img-responsive" src="resources/images/electronics/phones.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/3">
      <img class="imgrotate img-responsive" src="resources/images/electronics/laptops.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/5">
      <img class="imgrotate img-responsive" src="resources/images/electronics/games.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/4">
      <img class="imgrotate img-responsive" src="resources/images/electronics/camera.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
 </div>
  
  
  <div style="width: 100%;" class="row catimage-div"> 
    <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/2">
      <img class="imgrotate img-responsive" src="resources/images/electronics/tv.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/6">
      <img class="imgrotate img-responsive" src="resources/images/Men/shirts.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/7">
      <img class="imgrotate img-responsive" src="resources/images/Men/tshirts.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/8">
      <img class="imgrotate img-responsive" src="resources/images/Men/lowers.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
 </div>
  
  
  <div style="width: 100%;" class="row catimage-div"> 
    <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/10">
      <img class="imgrotate img-responsive" src="resources/images/Men/watches.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/9">
      <img class="imgrotate img-responsive" src="resources/images/Men/boots.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/11">
      <img class="imgrotate img-responsive" src="resources/images/women/shirts.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/13">
      <img class="imgrotate img-responsive" src="resources/images/women/tshirt.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
 </div>
  
  
  
  <div style="width: 100%;" class="row catimage-div"> 
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/12">
      <img class="imgrotate img-responsive" src="resources/images/women/jeans.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/14">
      <img class="imgrotate img-responsive" src="resources/images/women/boot.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/15">
      <img class="imgrotate img-responsive" src="resources/images/women/watch.jpg" class="img-responsive indexcatimage"  ></a>  
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/23">
      <img class="imgrotate img-responsive" src="resources/images/book/child.jpg" class="img-responsive indexcatimage" ></a>
  </div>
 </div>
 
 
   <div style="width: 100%;" class="row catimage-div"> 
  <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/20">
      <img class="imgrotate img-responsive" src="resources/images/book/academic.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/21">
      <img class="imgrotate img-responsive" src="resources/images/book/fiction1.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/22">
      <img class="imgrotate img-responsive" src="resources/images/book/drama.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
   <div align="center" class="col-sm-3">
      <a href="${pageContext.request.contextPath}/productbysubcat/23">
      <img class="imgrotate img-responsive" src="resources/images/book/comics.jpg" class="img-responsive indexcatimage"  ></a>
  </div>
 </div>
 
<div id="pointer">
  <p style="font-size:25px; font-weight:bold; font-family: 'family=Lora' ;" >&nbsp;&nbsp;Latest Products</p>
</div>

  <div class="row" style="width: 100%;">
    <br><br>
    <c:forEach items="${latestproduct}" var="x">   
    <div align="center" class="col-sm-3" >
      <a href="${pageContext.request.contextPath}/product/${x.getId()}"><img src="${x.getProductimage()}" class="img-responsive latestproduct"></a>
    </div>
    </c:forEach>
    <br>
  </div>
<jsp:include page="footer.jsp" />  
</body>
</html>