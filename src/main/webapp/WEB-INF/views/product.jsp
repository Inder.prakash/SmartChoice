<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="header.jsp"></jsp:include>
<br><br><br><br>

<Style>

.btn-circle.btn-lg {
  width: 50px;
  height: 50px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.33;
  border-radius: 25px;
}

.section-content{
  text-align: center; 

}

.form-line{
  height:700px;
  border-right: 1px solid #B29999;
}

</Style>

		<section id="contact">
		<form action="${pageContext.request.contextPath}/AddCartToDB" method="post">				
			<input type="hidden" name="id" value="${productdata.getId()}">
     		<input type="hidden" name="userid" value="${user.getId()}">
     		<input type="hidden" name="address" value="${user.getAddress1()}">
			<div class="contact-section">
			<div class="container">
					<div class="col-md-6 form-line">
			  			<img class="proimage"  width="350px" class="img-responsive" src="${productdata.getProductimage()}">
			  		</div>
			  		
			  		<div class="col-md-6">
			  			<div class="section-content">
							<h1 class="section-header">${productdata.getProductname()}</span></h1>
						</div>	
						<br><br>
			  			<div class="form-group">
			  				${productdata.getProductdescription()}	 
				  		</div>
				  		<table class="table">
				  		<Tr>
			  				<td>
			  				<div class="form-group"><h3>Rs.${productdata.getProductprice()}</h3></div>
			  				</td>
			  			</Tr>
			  			<Tr>
			  				<td>
			  				<div class="form-group"><h3>In Stock : ${productdata.getProductquantity()}</h3></div>
			  				</td>
			  			</Tr>
						</table>
					  	<div class="form-group">
					  	<br>
					  	<table style="width: 20px;" class="table">
					  	<tr >
					  		<td style="width: 10px;">
					  		<button type="button" onclick = "minus()" class="btn btn-primary btn-circle btn-lg">
      	 					<i class="glyphicon glyphicon-minus"></i></button>
					  		</td>
					  		<Td style="width: 20px;">
					  		<input style="width:50px;" type="text" class="form-control" readonly value="1" name="quantity" id="c">
					  		</Td>
					  		<Td style="width:  10px;">
					  		<button  type="button"  onclick = "plus(${productdata.getProductquantity()})" class="btn btn-primary btn-circle btn-lg">
      	 					<i class="glyphicon glyphicon-plus"></i></button>		  		
					  		</Td>
					  	</tr>
					  	</table>  	 					
			  			</div>
			  			<div class="form-group">
			  			<table class="table">
			  			<Tr>
			  				<td>
			  				<input style=" float:none;" class="btn btn-success" type="submit" value="Add to Cart">
			  				</td>
			  			</Tr>
			  			</table>
			  			</div>
			  		</div>
			  </div>
			</div>
			</form>
		</section>
			
<br><br><br><br><br><br><br><br>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html><script>
var count = 1;
var countEl = document.getElementById("c");
function plus(a){
  if(count < a) {
     count++;
    countEl.value = count;
  }
   
}
function minus(){
  if (count > 1) {
    count--;
    countEl.value = count;
  }  
}
</script>