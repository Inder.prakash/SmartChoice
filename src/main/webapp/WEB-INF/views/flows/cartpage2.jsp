<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cart Page2</title>
</head>
<jsp:include page="/WEB-INF/views/header.jsp"></jsp:include>
<body ng-app="myApp" ng-controller="myCtrl">

<br>
<br>
<a href="${flowExecutionUrl}&_eventId=viewItemsInCart" class="btn btn-success" style=" float:left; margin-left:50px; margin-top:45px">Previous</a>
<a href="${flowExecutionUrl}&_eventId=invoice" class="btn btn-success" style="float:right ;margin-right:50px; margin-top:10px">Next</a>
<script type="text/ng-template" id="loader">
<img style="width:250px" src='${pageContext.request.contextPath}/resources/images/loading.gif'>
</script>
<br>
<br>
<script>
var app = angular.module("myApp", ['ngMaterial']).controller("myCtrl", 
		['$scope','$http', '$mdDialog',
		    function( $scope,$http, $mdDialog ){
			
			$scope.showAdvanced = function() {
			    $mdDialog.show({
			      templateUrl: 'loader',
			      clickOutsideToClose:false
			    })
			  };
			  
    $scope.data = [];
    
    $scope.billadd='';
    $scope.shipadd='';

    $scope.done = false;
    $scope.showAdvanced();
    $http(	{
    			method: 'POST',
	      		url: 'getAddress',
	      		 headers : {'Content-Type':'application/x-www-form-urlencoded'}
    			}).then(function(response){
    				$mdDialog.cancel();
			    	console.log(response.data);
			    
			    	$scope.data = response.data;
			   
			    	$scope.billadd=response.data.shipaddr;
			        $scope.shipadd=response.data.billaddr;
			        
			        console.log( $scope.shipadd );
			        console.log( $scope.billadd );
    			});
    
 $scope.Update=function(){
    	
	 	var json = {"ship":$scope.shipadd,"bill":$scope.billadd};
	 	 $scope.showAdvanced();
    	$http(	{
			method: 'POST',
      		url: 'UpdateCartToDB',
      		data:JSON.stringify(json),
      		 headers : {'Content-Type':'application/json'}
			}).then(function(response){
				$mdDialog.cancel();
		    	console.log(response.data);
		    	
		    	if(response.data.msg=="Updated")
		    	{
		    		$scope.done = true;	    	
		    	}
		    	
		    
		    	
			});
    	
    }
}]);

</script>
<div>
<br>
<br>
<h1 align="center">Shipping Address:</h1>
<textarea style="width: 40%; margin: auto;" rows="5" name="shippingaddress"  class="form-control" ng-model="shipadd" ></textarea>
<br>
<br>
<br>
<h1 align="center">Billing Address:</h1>
<textarea style="width: 40%; margin: auto;" rows="5" name="billingaddress" class="form-control" ng-model="billadd" ></textarea>
<br>
<br>

</div>

<button ng-click="Update()" class="btn btn-success" style="float:left; margin-left:600px;  text-align: center;">Update</button>

<br>

<h1 class="text text-success" ng-show='done' style="margin: auto; width: 80%; text-align: center;">Updated Successfully</h1>
<br><br><br><br>
<jsp:include page="../footer.jsp"></jsp:include>
</body>
</body>
</html>