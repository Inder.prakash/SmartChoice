<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cart Page4</title>
</head>
<jsp:include page="/WEB-INF/views/header.jsp"></jsp:include>
<body ng-app="myApp" ng-controller="myCtrl">
<br>
<br>
<br>
<br>

<a href="ViewProducts" class="btn btn-success" style=" float:left; margin-left:50px; margin-top:45px">Buy More</a>

<h1 style="margin: auto; width: 80%; text-align: center;">Thank You </h1>
<h1 style="margin: auto; width: 80%; text-align: center;">Your Order is confirmed</h1>

<a href="${pageContext.request.contextPath}/" class="btn btn-success" style="float:right ;margin-right:50px; margin-top:10px">Home</a>

<br>
<br>
<br>
<br>

<script>

var app = angular.module("myApp", ['ngMaterial']).controller("myCtrl",
		['$scope','$http', '$mdDialog',
		    function( $scope,$http, $mdDialog ){
   
    
   $scope.data=[];
    
    $http(	{
    			method: 'POST',
	      		url: 'DeleteCartItems',
	      		 headers : {'Content-Type':'application/x-www-form-urlencoded'}
    			}).then(function(response){
			    	console.log(response.data);
			    
			    	
			    	
    			});
    
    $scope.Delete=function(arg){
    	
    	alert(arg);
    	
    	var json={"id":arg};
    	
    	console.log( JSON.stringify(json) );
    	
    	$http(	{
			method: 'POST',
      		url: 'deletefromcart',
      		data:JSON.stringify(json),
      		 headers : {'Content-Type':'application/json'}
			}).then(function(response){
		    	console.log(response.data);
		    
		    	//Fetch Again
		    	$http(	{
	    			method: 'POST',
		      		url: 'getAllCartItems',
		      		 headers : {'Content-Type':'application/x-www-form-urlencoded'}
	    			}).then(function(response){
				    	console.log(response.data);
				    
				    	$scope.data = response.data;
				    	
	    			});
		    	
			});
    	
    }
    
}]);


</script>
<br><br><br><br>
<jsp:include page="../footer.jsp"></jsp:include>
</body>