<%@page import="java.security.Principal"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel='stylesheet prefetch' href='https://cdn.gitcdn.link/cdn/angular/bower-material/v1.1.5/angular-material.css'>
<link rel='stylesheet prefetch' href='https://material.angularjs.org/1.1.5/docs.css'>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-messages.min.js"></script>
<script src='https://cdn.gitcdn.link/cdn/angular/bower-material/v1.1.5/angular-material.js'></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
      <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/MyCss.css">
<title>Insert title here</title>
</head>

<style>

@media (max-width: 1298px) {
    .navbar-header {
        float: none;
    }
    .navbar-left,.navbar-right {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
		top: 0;
		border-width: 0 0 1px;
	}
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
		margin-top: 7.5px;
	}
	.navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
  		display:block !important;
	}
}

</style>

<body>
<div> 
  <nav style="background-color: #0099cc; border-color: #0099cc;" class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a style="margin: 0px; padding: 0px;" class="navbar-brand" href="<c:url value="/index" />"><img  width="150px" src='${pageContext.request.contextPath}/resources/images/SmartChoiceLogo.png'></a>
    </div>
    <Style>
    .navbarmargin ul li li
    {
    	margin: 0px;
    }

    </Style>
    <div class="collapse navbar-collapse navbarmargin" id="myNavbar">
      <ul class="nav navbar-nav">
        <li style="margin: 0px;"  class="dropdown">
         <a style="color:white;font-weight:bold; font-size:14px;" class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-flash"> </span> ELECTRONICS<span class="caret"></span></a>
          <ul style="margin: 0px;" class="dropdown-menu">
            <li ><a href="${pageContext.request.contextPath}/productbysubcat/1">Phones</a></li>
            <li ><a href="${pageContext.request.contextPath}/productbysubcat/3">Laptop</a></li> 
            <li ><a href="${pageContext.request.contextPath}/productbysubcat/4">Camera</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/2">Television</a></li>   
            <li><a href="${pageContext.request.contextPath}/productbysubcat/5">Games</a></li>
          </ul>
        </li>
        
        
       <li style="margin: 0px;"  class="dropdown">
          <a style="color:white;font-weight:bold; font-size:14px;" class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-king"> </span> MEN<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="${pageContext.request.contextPath}/productbysubcat/6">Shirt</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/7">Tshirt</a></li> 
            <li><a href="${pageContext.request.contextPath}/productbysubcat/8">Jeans</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/9">Boot</a></li>   
            <li><a href="${pageContext.request.contextPath}/productbysubcat/10">Watches</a></li>
          </ul>
        </li>
        
        <li style="margin: 0px;"  class="dropdown">
          <a style="color:white;font-weight:bold; font-size:14px;" class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-queen"> </span> WOMEN<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="${pageContext.request.contextPath}/productbysubcat/11">Shirt</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/13">Tshirt</a></li> 
            <li><a href="${pageContext.request.contextPath}/productbysubcat/12">Jeans</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/14">Boot</a></li>   
            <li><a href="${pageContext.request.contextPath}/productbysubcat/15">Watches</a></li>
          </ul>
        </li>
        
        <li style="margin: 0px;"  class="dropdown">
          <a style="color:white;font-weight:bold; font-size:14px;" class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-book"> </span> BOOKS<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="${pageContext.request.contextPath}/productbysubcat/20">Academic</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/21">Science Fiction</a></li> 
            <li><a href="${pageContext.request.contextPath}/productbysubcat/22">Drama</a></li>
            <li><a href="${pageContext.request.contextPath}/productbysubcat/23">Comics</a></li>   
          </ul>
        </li>
        
        <li>
          <form action="${pageContext.request.contextPath}/Searchdata" method="post" class="navbar-form navbar-left">  
	           <div class="form-group">
	          	 <button type="submit" class="btn btn-info">Search</button>
	          </div>
	          <div class="form-group">
	         	 <input type="text" style="width: 250px;" class="form-control" name="keytag" placeholder="Search">
	          </div>  
          </form>
        </li>
            
       <%--  <li><a href="${pageContext.request.contextPath}/Aboutus">ABOUT US</a></li>
        <li><a href="${pageContext.request.contextPath}/Contatctus">CONTACT US</a></li> --%>
      </ul>
  	
      <ul class="nav navbar-nav navbar-right">
     <li style="margin: 0px;">
        <a style="color:white;font-weight:bold; font-size:14px; margin-top: 2px;"href="${pageContext.request.contextPath}/cart"><span class="glyphicon glyphicon-shopping-cart"></span>Cart</a>
      </li>  
      <c:choose>
      <c:when test="${userName==null}">
        <li style="margin: 0px;"><a style="color:white;font-weight:bold; font-size:14px;" href="${pageContext.request.contextPath}/SignUP"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li style="margin: 0px;"><a style="color:white;font-weight:bold; font-size:14px;" href="${pageContext.request.contextPath}/loginpage"><span class="glyphicon glyphicon-off"></span> Login</a></li>
      </c:when>
      <c:otherwise>
          
         <li style="margin: 0px;"  class="dropdown">
          <a style="color:white;font-weight:bold; font-size:14px;" class="dropdown-toggle" data-toggle="dropdown" href="#">Welcome ${userName.getFirstname()}<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li style="margin: 0px;"><a href="${pageContext.request.contextPath}/MyProfiles/${userName.getId()}">View Profile</a></li>
            <li style="margin: 0px;"><a href="${pageContext.request.contextPath}/UpdateProfile/${userName.getId()}">Edit Profile</a></li>           
          </ul>
        </li>           
        <li style="margin: 0px;">
        <a style="color:white;font-weight:bold; font-size:14px;" href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
        </li>
      </c:otherwise>
      </c:choose>
      </ul>
      
      
    </div>
  </div>
</nav>

</div>


</body>
</html>