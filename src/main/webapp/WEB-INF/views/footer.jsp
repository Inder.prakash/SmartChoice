 <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<div  id="body">
<!-- Container (Services Section) -->
<div style="color: white;" class="container-fluid text-center">
 <br> <br><br>
  <img src="${pageContext.request.contextPath}/resources/images/services.png">
  <h4>What we offer</h4>
  <br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-off logo-small"></span>
      <h4>POWER</h4>
      <p>Best manpower quotes selected by thousands of our users!</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-heart logo-small"></span>
      <h4>LOVE</h4>
      <p>Showing customers the love and respect they deserve has never been more critical.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-lock logo-small"></span>
      <h4>JOB DONE</h4>
      <p>Choose a job you love, and you will never have to work a day in your life.</p>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-leaf logo-small"></span>
      <h4>GREEN</h4>
      <p>The future will either be green or not at all.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-certificate logo-small"></span>
      <h4>CERTIFIED</h4>
      <p>Officially recognize as possessing certain qualifications or meeting certain standards.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-wrench logo-small"></span>
      <h4>HARD WORK</h4>
      <p>Some people dream of success while others wake up and work hard at it.</p>
    </div>
  </div>
</div>


<!-- Container (Contact Section) -->
<div style="color: white;" class="container-fluid bg-grey">
<div class="container">
<br>
  <h2 style="margin-top: 80px;" class="text-center">CONTACT</h2>
  <div " class="row">
    <div class="col-sm-5">
    <br><br>
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> India , New Delhi</p>
      <p><span class="glyphicon glyphicon-phone"></span>97169*****</p>
      <p><span class="glyphicon glyphicon-envelope"></span> Inder*****@hotmail.com</p> 
    </div>
    <div class="col-sm-7">
    <form id="mailform" action="mail" method="post">
      <div class="row">
      <br><br>
      
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <div class="row">
      	<div class="col-sm-12 form-group">
      		<textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
      	</div> 
      </div>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button type="submit" form="mailform" class="btn btn-info submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Send</button>
      	</div> 
      </div> 	
      </form>
      <c:if test="${msg != null}">
	  		 <script type="text/javascript">
	  		 	swal("Message Successfully Sent");
	  		 </script>
  	  </c:if>
    </div>
  </div>
  </div>
</div>
<br>
<br>
<br>
<br>
</div>