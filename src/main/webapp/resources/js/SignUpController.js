 
var app = angular.module("myapp",[]);
app.controller("SignUpController",['$scope','$location','$window','$http',function( $scope , $location, $window , $http){


	$scope.firstname = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var reg = /\S+/;
				this.error = !reg.test( this.value );
				var size = /^.{3,8}$/;
				this.length =  !size.test( this.value );
			}
	}
	
	$scope.lastname = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var reg = /\S+/;
				this.error = !reg.test( this.value );
				var size = /^.{3,8}$/;
				this.length =  !size.test( this.value );
			}
	}
	
	$scope.email = 
	{ 
			
			value: '',
			error : true,
			touched : false,
			validate: function(){
				var ch = "";
				this.touched = true;
				var emp = /\S+/;
				this.empty = !emp.test( this.value );
				var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
				this.error = !reg.test( this.value );					
			}
	}
	
	$scope.chk = function () {
		var json={"email":$scope.email.value};
		console.log($scope.email.value);
		$http({
			method:'post',
			url:'check',
			data: json,
			headers:{'Content-Type':'application/json'}
		}).then(function(response) {	
			$scope.email.chk = ( response.data.msg == "Error");	
		});	
	}

	$scope.password = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var emp = /\S+/;
				this.empty = !emp.test( this.value );
				var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
				this.error = !reg.test( this.value );
				$scope.cpassword.validate();
			}
	}
	
	$scope.cpassword = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				this.match = (this.value != $scope.password.value);
			}
	}
	
	$scope.phone = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var emp = /\S+/;
				this.empty = !emp.test( this.value );
				var reg = /^[7-9]{1,1}[0-9]{9,9}$/;
				this.error = !reg.test( this.value );
			}
	}
	
	$scope.faddress = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var emp = /\S+/;
				this.empty = !emp.test( this.value );
			}
	}
	
	$scope.city = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				this.error = ( this.value == null);
				
			}
	}
	
	$scope.pin = 
	{ 
			value: '',
			error : true,
			touched : false,
			validate: function(){
				this.touched = true;
				var emp = /\S+/;
				this.empty = !emp.test( this.value );
				var reg = /^[1-9]{1,1}[0-9]{5,5}$/;
				this.error = !reg.test( this.value );
			}
	}

}]);